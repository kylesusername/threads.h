threads.h
=========

C11 threads bindings using pthread
----------------------------------

My irritation that glibc hasn't implemented C11 threads yet
led me to do some research, and then to create bindings for
C11 threads over pthreads.
